#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define SEARCH_AREA 28
#define HIGHEST 119

typedef struct{
  //2本の柱の座標を配列に格納
  int x[2];
  int y;
  int z[2];

  //ゲートの内側の幅を格納
  int width;
}OverWorld;

typedef struct{
  //2本の柱の座標を配列に格納
  int x[2];
  int y;
  int z[2];

  //ゲートの内側の幅を格納
  int width;

  //エラーの有無
  int errer;
}NetherWorld;

typedef struct{
  //基準点
  double reference_x[2];
  double reference_y[2];
  double reference_z[2];

  //中点
  double midpoint_x;
  double midpoint_y;
  double midpoint_z;

  //幅・高さ格納
  double hight;
  double width;

  //ファイル
  FILE *read_log;
  FILE *write_log;
  FILE *read_config;
  FILE *write_config;

  //ファイル入力判断
  int input_flag;

  //デバグモード
  int mode;
}CalcInfo;

int double_to_int(double a){ //整数値変換
  if(a != (int)a){
    if(a >= 0) return (int)a;
    if(a < 0) return (int)(a-1.0);
  }
  return a;
}

double distance(double a,double b,double c,double d,double e,double f){ //２点間の距離
  return sqrt(pow(a-b,2)+pow(c-d,2)+pow(e-f,2));
}

void nethergate_print(){
  printf("\n");
  printf("   ■ ■ ■ ■\n");
  printf("   ■     ■\n");
  printf("   ■     ■\n");
  printf("   ■     ■\n");
  printf("   ■ ■ ■ ■\n");
}

void inputerrer_print(){
  int clear;
  printf("入力が不正です。数字を入力してください。\n>");
  while ((clear = getchar()) != '\n'); //scanfバッファのクリア
}

int overlap_check(OverWorld gate[]){

  int flag=0,i;

  if(gate[1].y == gate[0].y) flag++;

  for(i = 0;i < 2;i++){
    if(gate[1].x[0] == gate[0].x[i]) flag++;
    if(gate[1].z[0] == gate[0].z[i]) flag++;
    if(gate[1].x[1] == gate[0].x[i]) flag++;
    if(gate[1].z[1] == gate[0].z[i]) flag++;
  }

  if(flag >= 7) return 1;

  return 0;

}

void input_gateinfo(OverWorld gate[]){

  double temp;
  int select,i;

  for(i = 0;i < 2;i++){

    printf("\n////////////////////////////////////////////////////////////////\n");
    printf("\nネザーゲート%d\n",i+1);

    nethergate_print();
    printf(" 　↑");
    printf("\n\n 矢印が示すブロックの座標を入力してください。\n");
    printf(" ※ 二ヶ所ありますがどちらでも構いません\n\n");

    printf(" X座標：");
    while(!scanf("%lf",&temp)) inputerrer_print();
    gate[i].x[0] = double_to_int(temp);

    printf(" Y座標：");
    do{
      while(!scanf("%lf",&temp)) inputerrer_print();
      if(temp < 0 || temp > 252) printf("ゲートの高さがおかしいです。もう一度入力してください。\n>");
    }while(temp < 0 || temp > 252);
    gate[i].y = double_to_int(temp);

    printf(" Z座標：");
    while(!scanf("%lf",&temp)) inputerrer_print();
    gate[i].z[0] = double_to_int(temp);

    printf("\n////////////////////////////////////////////////////////////////\n");

    printf("\n");
    nethergate_print();
    printf(" 　↑     ↑\n");
    printf("\n 2つの矢印が示すブロックの間にあるブロック数を入力してください。\n\n");
    printf(" ブロック数(幅)：");

    do{
      while(!scanf("%d",&gate[i].width)) inputerrer_print();
      if(gate[i].width < 2 || gate[i].width > 21) printf("ゲート幅がおかしいです。もう一度入力してください(2~21)。\n>");
    }while(gate[i].width < 2 || gate[i].width > 21);

    printf("\n////////////////////////////////////////////////////////////////\n");

    nethergate_print();
    printf(" 　      ↑\n");
    printf("\n\n 矢印が示すブロックの座標を以下の選択肢から数字で選択してください。\n\n");

    printf("         X     Y     Z\n");
    printf(" (1) (%5d, %5d, %5d)\n",gate[i].x[0]-gate[i].width-1,gate[i].y,gate[i].z[0]);
    printf(" (2) (%5d, %5d, %5d)\n",gate[i].x[0]+gate[i].width+1,gate[i].y,gate[i].z[0]);
    printf(" (3) (%5d, %5d, %5d)\n",gate[i].x[0],gate[i].y,gate[i].z[0]-gate[i].width-1);
    printf(" (4) (%5d, %5d, %5d)\n",gate[i].x[0],gate[i].y,gate[i].z[0]+gate[i].width+1);
    printf("\n>");

    do{
      while(!scanf("%d",&select)) inputerrer_print();

      switch (select) {
        case 1:
        gate[i].x[1] = gate[i].x[0]-gate[i].width-1;
        gate[i].z[1] = gate[i].z[0];
        break;

        case 2:
        gate[i].x[1] = gate[i].x[0]+gate[i].width+1;
        gate[i].z[1] = gate[i].z[0];
        break;

        case 3:
        gate[i].x[1] = gate[i].x[0];
        gate[i].z[1] = gate[i].z[0]-gate[i].width-1;
        break;

        case 4:
        gate[i].x[1] = gate[i].x[0];
        gate[i].z[1] = gate[i].z[0]+gate[i].width+1;
        break;

        default:
        select = 0;
        printf("もう一度入力してください(1~4)。\n>");

      }
    }while(!select);

  }

  //重複チェック
  if(overlap_check(gate)){
    printf("\n\n\n　2つのゲートのゲートの座標が同じです。もう一度入力してください。\n");

    input_gateinfo(gate);
  }

}

void output_gateinfo(NetherWorld gate[]){

  int i,n;

  printf("\n結果:");

  if(gate[0].errer){
    printf("最適化失敗\n");
    printf("\n　残念ながら、現在のネザーゲートの条件では最適化できませんでした。\n\n");
    printf("　ネザーゲート同士を離す、高度を下げるなど条件の変更をお願いします。\n\n");
    printf("\n※ お手数ですが、プログラム製作者に入力した座標をTwitterのDMやコメントで報告をお願いします。\n");
  }else{
    printf("最適化成功\n");

    for(i = 0;i < 2;i++){
      printf("\nゲート%d\n",i+1);
      if(gate[i].y == -1) printf("Y:高さ自由\n");
      if(gate[i].y != -1) printf("Y:%d\n",gate[i].y);

      for(n = 0;n < 2;n++){
        printf("(%d ,",gate[i].x[n]);
        printf("%d)\n",gate[i].z[n]);
      }

    }
  }

  printf("\n");
  printf("終了するには何かを入力してください...\n");
  scanf("%d",&i);
  printf("\n");

}

///////////////////////calc////////////////////////

double nether_point(double a){ //ネザーワールドの座標に変換
  return a/8;
}

void sort(OverWorld calc_gate[]){ //大小関係を固定([1]の数値が[0]より大きくなるようソート)

  int i;
  double temp;

  for(i = 0;i < 2;i++){

    if(calc_gate[i].x[0] > calc_gate[i].x[1] && calc_gate[i].z[0] == calc_gate[i].z[1]){
      temp = calc_gate[i].x[0];
      calc_gate[i].x[0] = calc_gate[i].x[1];
      calc_gate[i].x[1] = temp;
    }
    if(calc_gate[i].z[0] > calc_gate[i].z[1] && calc_gate[i].x[0] == calc_gate[i].x[1]){
      temp = calc_gate[i].z[0];
      calc_gate[i].z[0] = calc_gate[i].z[1];
      calc_gate[i].z[1] = temp;
    }
  }

  if(calc_gate[1].z[1] < calc_gate[0].z[1]){

    for(i = 0;i < 2;i++){

      temp = calc_gate[1].x[i];
      calc_gate[1].x[i] = calc_gate[0].x[i];
      calc_gate[0].x[i] = temp;

      temp = calc_gate[1].z[i];
      calc_gate[1].z[i] = calc_gate[0].z[i];
      calc_gate[0].z[i] = temp;

    }

    temp = calc_gate[1].y;
    calc_gate[1].y = calc_gate[0].y;
    calc_gate[0].y = temp;

  }

}

void reference_point(OverWorld calc_gate[], CalcInfo *calc){ //基準点

  //一番遠い２箇所のポータルブロックの座標を基準点とする
  int i,n;
  double max = 0;

  sort(calc_gate);

  for(i = 0;i < 2;i++){
    //柱の座標から最外郭のポータルブロック座標へ変更
    if(calc_gate[i].x[0] != calc_gate[i].x[1]){
      calc_gate[i].x[0] += 1;
      calc_gate[i].x[1] -= 1;
    }
    if(calc_gate[i].z[0] != calc_gate[i].z[1]){
      calc_gate[i].z[0] += 1;
      calc_gate[i].z[1] -= 1;
    }
  }

  //一番遠い二箇所のポータルブロックを決定
  for(i = 0;i < 2;i++){
    for(n = 0;n < 2;n++){
      if(max < distance(calc_gate[0].x[i],calc_gate[1].x[n],calc_gate[0].z[i],calc_gate[1].z[n],0,0)){
        max = distance(calc_gate[0].x[i],calc_gate[1].x[n],calc_gate[0].z[i],calc_gate[1].z[n],0,0);
        calc->reference_x[0] = nether_point(calc_gate[0].x[i]);
        calc->reference_z[0] = nether_point(calc_gate[0].z[i]);
        calc->reference_x[1] = nether_point(calc_gate[1].x[n]);
        calc->reference_z[1] = nether_point(calc_gate[1].z[n]);
      }
    }
  }

  calc->reference_y[0] = (double)calc_gate[0].y;
  calc->reference_y[1] = (double)calc_gate[1].y;

}

void midpoint(CalcInfo *calc){ //中点

  calc->midpoint_x = (calc->reference_x[0] + calc->reference_x[1])/2.0;
  calc->midpoint_y = (calc->reference_y[0] + calc->reference_y[1])/2.0;
  calc->midpoint_z = (calc->reference_z[0] + calc->reference_z[1])/2.0;

}

int overarea(CalcInfo *calc){ //２つのゲートの探索範囲の重なり度合いを判断

  calc->hight = abs(double_to_int(calc->reference_x[1] - calc->reference_x[0]));
  calc->width = abs(double_to_int(calc->reference_z[1] - calc->reference_z[0]));

  if((calc->hight <= SEARCH_AREA+1 || calc->width <= SEARCH_AREA+1) && calc->hight * calc->width < pow(SEARCH_AREA/2+1,2)) return 1;
  return 0;
}

void case1(NetherWorld result_gate[], CalcInfo *calc){

  int i,count;
  double vector_x,vector_y,vector_z;
  double X,Z;
  double min_distance;
  double min_x,min_z;
  double intercept;

  count = 0;

  if(calc->midpoint_y > HIGHEST){ //中点の高さがネザーの天井岩盤より高い場合

    vector_x = calc->reference_x[1]*8 - calc->reference_x[0]*8;
    vector_y = calc->reference_y[1] - calc->reference_y[0];
    vector_z = calc->reference_z[1]*8 - calc->reference_z[0]*8;

    intercept = -vector_x*calc->midpoint_x*8-vector_y*calc->midpoint_y-vector_z*calc->midpoint_z*8;

    if(vector_x == 0){

      Z = ((1/vector_z) * (-vector_y * HIGHEST - intercept))/8;

      if(distance(Z,calc->midpoint_z,0,0,0,0) < SEARCH_AREA/2){
        min_x = calc->midpoint_x;
        min_z = Z;
        count = 1;
      }else{
        count = 0;
      }

    }

    if(vector_x != 0){

      min_distance = (SEARCH_AREA * sqrt(2)) / 2;

      for(Z = (*calc).midpoint_z-SEARCH_AREA/2; Z <= (*calc).midpoint_z+SEARCH_AREA/2; Z++){

        X = ((1/vector_x)*(-vector_y*HIGHEST-vector_z*Z-intercept))/8;

        if(calc->midpoint_x+(SEARCH_AREA/2) > X && calc->midpoint_x-(SEARCH_AREA/2) < X){ //探索範囲内だった場合

          if(min_distance > distance(X,calc->midpoint_x,Z,calc->midpoint_z,0,0)){
            min_distance = distance(X,calc->midpoint_x,Z,calc->midpoint_z,0,0);
            min_x = X;
            min_z = Z;
            count++;
          }
        }
      }
    }

    if(count == 0){
      result_gate[0].errer = 1;
      return;
    }else{
      calc->midpoint_x = min_x;
      calc->midpoint_y = HIGHEST;
      calc->midpoint_z = min_z;
    }

  }

  //ゲートの向きを決定
  if(abs(calc->reference_x[1]-calc->reference_x[0]) > abs(calc->reference_z[1]-calc->reference_z[0])){

    for(i = 0;i < 2;i++){
      result_gate[0].x[i] = double_to_int(calc->midpoint_x - 1);
      result_gate[1].x[i] = double_to_int(calc->midpoint_x + 1);
      result_gate[i].z[0] = double_to_int(calc->midpoint_z - 1);
      result_gate[i].z[1] = double_to_int(calc->midpoint_z + 2);
    }

  }else{

    for(i = 0;i < 2;i++){
      result_gate[0].z[i] = double_to_int(calc->midpoint_z - 1);
      result_gate[1].z[i] = double_to_int(calc->midpoint_z + 1);
      result_gate[i].x[0] = double_to_int(calc->midpoint_x - 1);
      result_gate[i].x[1] = double_to_int(calc->midpoint_x + 2);
    }

  }

  //Y座標の決定
  if((calc->reference_x[0] == calc->reference_x[1] && calc->reference_z[1] == calc->reference_z[0]) || count){
    result_gate[0].y = double_to_int(calc->midpoint_y - 1);
    result_gate[1].y = double_to_int(calc->midpoint_y);
  }else{
    result_gate[0].y = double_to_int(calc->midpoint_y);
    result_gate[1].y = double_to_int(calc->midpoint_y);
  }

}


void case2(NetherWorld result_gate[], CalcInfo *calc){

  int i,flag;
  double shiftpoint;

  if(calc->midpoint_z - calc->reference_z[0] != 0){

    shiftpoint = (SEARCH_AREA / 2) * tan(atan((calc->midpoint_x - calc->reference_x[0]) / (calc->midpoint_z - calc->reference_z[0])));

    for(i = 0;i < 2;i++){

      //ゲートの設置位置の決定
      calc->hight = calc->reference_x[1] - calc->reference_x[0];
      calc->width = calc->reference_z[1] - calc->reference_z[0];

      if(calc->hight < calc->width){
        if(i == 0) flag = 1;
        if(i == 1) flag = 3;
      }

      if(calc->hight >= calc->width){
        if(i == 0 && calc->reference_x[1] > calc->reference_x[0]) flag = 0;
        if(i == 0 && calc->reference_x[1] < calc->reference_x[0]) flag = 2;
        if(i == 1 && calc->reference_x[1] > calc->reference_x[0]) flag = 2;
        if(i == 1 && calc->reference_x[1] < calc->reference_x[0]) flag = 0;
      }

      //ゲートの向きを決定
      switch (flag) {
        case 0:
        case 2:
        if(flag == 0){
          result_gate[i].x[0] = double_to_int(calc->reference_x[i] + SEARCH_AREA / 2);
          result_gate[i].x[1] = double_to_int(calc->reference_x[i] + SEARCH_AREA / 2);
        }else{
          result_gate[i].x[0] = double_to_int(calc->reference_x[i] - SEARCH_AREA / 2);
          result_gate[i].x[1] = double_to_int(calc->reference_x[i] - SEARCH_AREA / 2);
        }

        if(i == 0){
          result_gate[i].z[0] = double_to_int(calc->reference_z[i] + shiftpoint - 2);
          result_gate[i].z[1] = double_to_int(calc->reference_z[i] + shiftpoint + 1);
        }else{
          result_gate[i].z[0] = double_to_int(calc->reference_z[i] - shiftpoint - 1);
          result_gate[i].z[1] = double_to_int(calc->reference_z[i] - shiftpoint + 2);
        }
        break;


        case 1:
        case 3:
        if(flag == 1){
          result_gate[i].z[0] = double_to_int(calc->reference_z[i] + SEARCH_AREA / 2);
          result_gate[i].z[1] = double_to_int(calc->reference_z[i] + SEARCH_AREA / 2);
        }else{
          result_gate[i].z[0] = double_to_int(calc->reference_z[i] - SEARCH_AREA / 2);
          result_gate[i].z[1] = double_to_int(calc->reference_z[i] - SEARCH_AREA / 2);
        }

        if(calc->reference_x[i] < calc->midpoint_x){
          result_gate[i].x[0] = double_to_int(calc->reference_x[i] + shiftpoint - 2);
          result_gate[i].x[1] = double_to_int(calc->reference_x[i] + shiftpoint + 1);
        }else{
          result_gate[i].x[0] = double_to_int(calc->reference_x[i] - shiftpoint - 1);
          result_gate[i].x[1] = double_to_int(calc->reference_x[i] - shiftpoint + 2);
        }
        break;
      }

    }

  }else{

    //X軸にそってゲートがある場合
    if(calc->reference_x[0] < calc->midpoint_x){
      result_gate[0].x[0] = double_to_int(calc->reference_x[0] + SEARCH_AREA / 2);
      result_gate[1].x[0] = double_to_int(calc->reference_x[1] - SEARCH_AREA / 2);
    }else{
      result_gate[0].x[0] = double_to_int(calc->reference_x[0] - SEARCH_AREA / 2);
      result_gate[1].x[0] = double_to_int(calc->reference_x[1] + SEARCH_AREA / 2);
    }

    for(i = 0;i < 2;i++){
      result_gate[i].z[0] = double_to_int(calc->reference_z[i] - 1);
      result_gate[i].z[1] = double_to_int(calc->reference_z[i] + 2);
      result_gate[i].x[1] = result_gate[i].x[0];
    }

  }

  //Y座標は任意
  result_gate[0].y = -1;
  result_gate[1].y = -1;

}

void makingfire(CalcInfo *calc){

  printf("\nコンフィグファイルを作成します。\n");
  calc->write_config = fopen("config.cfg","w");

  //configファイルに記述
  fprintf(calc->write_config, "///config////\n");
  fprintf(calc->write_config, "ゲート座標を入力してください（x,y,z）。\n");
  fprintf(calc->write_config, "（configの座標を使わない場合は、すべての座標を0にするか、このファイルを削除してください）\n");
  fprintf(calc->write_config, "※スペースは入れないでください\n");
  fprintf(calc->write_config, "・gate1\n");
  fprintf(calc->write_config, "(0,0,0) //柱A\n");
  fprintf(calc->write_config, "(0,0,0) //柱B\n");
  fprintf(calc->write_config, "\n");
  fprintf(calc->write_config, "・gate2\n");
  fprintf(calc->write_config, "(0,0,0) //柱A\n");
  fprintf(calc->write_config, "(0,0,0) //柱B\n");
  fprintf(calc->write_config, "\n");
  fprintf(calc->write_config, "\n");
  fprintf(calc->write_config, "・デバックモード[有効(1)/無効(0)] （特にいじる必要はありません）\n");
  fprintf(calc->write_config, "0\n");

  fclose(calc->write_config);

}

void fireinput(CalcInfo *calc,OverWorld gate[]){

  int n=0;
  char c;
  char buffer[256];
  int mode_count=0;
  int zero_count = 0;
  double input_date[4][3];
  calc->input_flag = 0;
  calc->mode = 0;

  if((calc->read_config = fopen("config.cfg","r")) == NULL) makingfire(calc); //ファイルが存在しない場合

  calc->read_config = fopen("config.cfg","r");

  while(fgets(buffer,256,calc->read_config) != NULL){

    if(sscanf(buffer,"%c%lf%c%lf%c%lf",&c,&input_date[n][0],&c,&input_date[n][1],&c,&input_date[n][2]) == 6){
      if(input_date[n][0] == 0 && input_date[n][1] == 0 && input_date[n][2] == 0) zero_count++;
      n++;
    }

    if(n == 4 && sscanf(buffer,"%d",&calc->mode) == 1) mode_count++;

  }

  if(mode_count != 1 || n != 4) makingfire(calc); //不正記述は初期化

  if(zero_count < 3 && n == 4 && mode_count == 1){
    gate[0].x[0] = double_to_int(input_date[0][0]);
    gate[0].y = double_to_int(input_date[0][1]);
    gate[0].z[0] = double_to_int(input_date[0][2]);

    gate[0].x[1] = double_to_int(input_date[1][0]);
    gate[0].y = double_to_int(input_date[1][1]);
    gate[0].z[1] = double_to_int(input_date[1][2]);

    gate[1].x[0] = double_to_int(input_date[2][0]);
    gate[1].y = double_to_int(input_date[2][1]);
    gate[1].z[0] = double_to_int(input_date[2][2]);

    gate[1].x[1] = double_to_int(input_date[3][0]);
    gate[1].y = double_to_int(input_date[3][1]);
    gate[1].z[1] = double_to_int(input_date[3][2]);

    calc->input_flag = 1;

    //重複チェック
    if(overlap_check(gate)){
      printf("\n\n\n　2つのゲートのゲートの座標が同じです。もう一度入力してください。\n");

      input_gateinfo(gate);
    }
  }

  fclose(calc->read_config);

}

void fireoutput(CalcInfo *calc,OverWorld gate[]){

  calc->write_log = fopen("log.txt","w");

  fprintf(calc->write_log,"(%d, %d, %d)\n",gate[0].x[0],gate[0].y,gate[0].z[0]);
  fprintf(calc->write_log,"(%d, %d, %d)\n",gate[0].x[1],gate[0].y,gate[0].z[1]);
  fprintf(calc->write_log,"(%d, %d, %d)\n",gate[1].x[0],gate[1].y,gate[1].z[0]);
  fprintf(calc->write_log,"(%d, %d, %d)\n",gate[1].x[1],gate[1].y,gate[1].z[1]);
  fprintf(calc->write_log,"%f, %f, %f\n",calc->reference_x[0],calc->reference_y[0],calc->reference_z[0]);
  fprintf(calc->write_log,"%f, %f, %f\n",calc->reference_x[1],calc->reference_y[1],calc->reference_z[1]);
  fprintf(calc->write_log,"%f * %f\n",calc->hight,calc->width);


  fclose(calc->write_log);
}



void calc(OverWorld calc_gate[], NetherWorld result_gate[], CalcInfo *calc){

  result_gate[0].errer = 0;

  reference_point(calc_gate,calc); //基準点の決定

  midpoint(calc); //中点の決定

  //ネザーワールドでのゲート探索範囲が重なっている場合
  if(overarea(calc)) case1(result_gate,calc);

  //２つのゲートの探索範囲が一定以上重なっていない場合
  if(!overarea(calc)) case2(result_gate,calc);

}

int main(){

  OverWorld O_gate[2]; //入力内容を格納
  NetherWorld N_gate[2]; //計算結果を格納
  CalcInfo calcinfo; //計算で必要な変数を格納

  printf("\n");
  printf("□ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □\n");
  printf("□                                               □\n");
  printf("□     ネザーゲート最適化ツール(Linux)           □\n");
  printf("□　                            Ver.1.03         □\n");
  printf("□           制作:Migly(みぐりー)                □\n");
  printf("□                                               □\n");
  printf("□ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □ □\n");

  fireinput(&calcinfo,O_gate);

  if(!calcinfo.input_flag) input_gateinfo(O_gate);

  calc(O_gate,N_gate,&calcinfo);

  output_gateinfo(N_gate);

  if(calcinfo.mode) fireoutput(&calcinfo,O_gate);

  return 0;
}
